<?php

namespace Lexik\Bundle\UiBundle\Bundle\Pagerfanta\View;

use Pagerfanta\View\ViewInterface;
use Pagerfanta\PagerfantaInterface;

use Symfony\Component\Translation\TranslatorInterface;

if ( ! interface_exists('Pagerfanta\View\ViewInterface')) {
    return;
}

class DefaultView implements ViewInterface
{
    /**
     * @var Translator $translator
     */
    protected $translator;

    /**
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Translate a key.
     *
     * @param strong $key
     * @param array  $parameters
     *
     * @return string
     */
    protected function trans($key, array $parameters = array())
    {
        return $this->translator->trans($key, $parameters, 'LexikUiBundle');
    }

    /**
     * {@inheritdoc}
     */
    public function render(PagerfantaInterface $pagerfanta, $routeGenerator, array $options = array())
    {
        $options = array_merge(array(
            'result_range' => false,
        ), $options);

        $currentPage = $pagerfanta->getCurrentPage();

        $startPage = 1;
        $endPage = $pagerfanta->getNbPages();
        if ($endPage > $pagerfanta->getNbPages()) {
            $startPage = max($startPage - ($endPage - $pagerfanta->getNbPages()), 1);
            $endPage = $pagerfanta->getNbPages();
        }

        // retrieve result range
        $first = ($currentPage * $pagerfanta->getMaxPerPage() - $pagerfanta->getMaxPerPage() + 1);
        $last = $first + $pagerfanta->getMaxPerPage() - 1;
        $last = ($last > $pagerfanta->getNbResults()) ? $pagerfanta->getNbResults() : $last;

        $pages = array();

        // result range
        if ($options['result_range']) {
            $pages[] = sprintf('<li class="lx-pager-result-range">%s</li>', $this->trans('pager.result_range', array(
                '%start%' => $first,
                '%end%'   => $last,
                '%total%' => $pagerfanta->getNbResults(),
            )));
        } else {
            $pages[] = sprintf('<li class="lx-pager-nbresults">%s</li>', $this->trans('pager.nbresults', array(
                '%total%' => $pagerfanta->getNbResults(),
            )));
        }

        if ($pagerfanta->getNbPages() > 1) {
            // first
            $class = ($currentPage == 1) ? 'ui-icon-arrowthickstop-1-w ui-state-disabled' : 'ui-icon-arrowthickstop-1-w';
            $pages[] = $this->renderLink('pager.first', $routeGenerator($startPage), $class);

            // previous
            $class = ($pagerfanta->hasPreviousPage()) ? 'ui-icon-arrowthick-1-w' : 'ui-icon-arrowthick-1-w ui-state-disabled';
            $pages[] = $this->renderLink('pager.previous', ($pagerfanta->hasPreviousPage()) ? $routeGenerator($pagerfanta->getPreviousPage()) : $routeGenerator($startPage), $class);

            // pages selector
            $pages[] = sprintf('<li><form method="get"><input type="text" name="page" value="%s" class="lx-pager-current-page ui-corner-all"> / %s</form></li>', $currentPage, $pagerfanta->getNbPages());

            // next
            $class = ($pagerfanta->hasNextPage()) ? 'ui-icon-arrowthick-1-e' : 'ui-icon-arrowthick-1-e ui-state-disabled';
            $pages[] = $this->renderLink('pager.next', ($pagerfanta->hasNextPage()) ? $routeGenerator($pagerfanta->getNextPage()) : $routeGenerator($endPage), $class);

            // last
            $class = ($currentPage < $pagerfanta->getNbPages()) ? 'ui-icon-arrowthickstop-1-e' : 'ui-icon-arrowthickstop-1-e ui-state-disabled';
            $pages[] = $this->renderLink('pager.last', $routeGenerator($endPage), $class);
        }

        return sprintf('<ul class="lx-pager">%s</ul>', implode('', $pages));
    }

    protected function renderLink($label, $url, $icon)
    {
        return sprintf('<li class="ui-state-default ui-corner-all"><a title="%s" href="%s"><span class="ui-icon %s"></span></a></li>',
            $this->trans($label),
            $url,
            $icon
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexikui';
    }
}
