<?php

namespace Lexik\Bundle\UiBundle\Bundle\SonataAdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin as SonataAdmin;

if ( ! class_exists('SonataAdmin')) {
    return;
}

class Admin extends SonataAdmin
{
    /**
     * (non-PHPdoc)
     * @see Sonata\AdminBundle\Admin.Admin::getListTemplate()
     */
    public function getListTemplate()
    {
        return 'LexikUiBundle:SonataAdminBundle/CRUD:list.html.twig';
    }

    /**
     * (non-PHPdoc)
     * @see Sonata\AdminBundle\Admin.Admin::getEditTemplate()
     */
    public function getEditTemplate()
    {
        return 'LexikUiBundle:SonataAdminBundle/CRUD:edit.html.twig';
    }
}
