/**
 * Toggle content jQuery plugin.
 *
 * Depends:
 *     jQuery
 *     jQuery UI ThemeRoller
 *
 * Basic usage:
 *     <a href="#" data-toggle-content>Toggle link</a>
 *     <div>Toggled content</div>
 *
 * Advanced usage:
 *     <a href="#" data-toggle-content="#toggle-element">Toggle link</a>
 *     (...)
 *     <div id="toggle-element">Toggled content</div>
 *
 * @author Jeremy Barthe <j.barthe@lexik.fr>
 */

(function($){
    $.fn.lxToggleContent = function(options) {

        var defaults = {
            iconShow: 'icon-minus',
            iconHide: 'icon-plus'
        };

        var settings = $.extend(defaults, options);

        function init() {
            var self = $(this);

            var isShow        = isDisplayed(self);
            var targetElement = getTargetElement($(this));

            // if a form is filled on target element, show the content
            if ( ! isShow && targetElement.find('[type=text][value!=""], textarea:not(:empty), :checkbox:checked, :radio:checked, select:has(option:selected[value!=""])').length > 0) {
                isShow = true;
                targetElement.show();
            }

            self
                .prepend('<i class="' + getToggleIconClassName(isShow) + '"></i>')
                .bind('click', function(e) {
                    e.preventDefault()

                    getTargetElement(self).toggle();
                    var isShow = isDisplayed(self);
                    $(this).find('i')
                           .addClass(getToggleIconClassName(isShow))
                           .removeClass(getToggleIconClassName( ! isShow));
                })
            ;
        }

        function getTargetElement(element) {
            var targetElement = element.attr('data-toggle-content');
            if (targetElement != '') {
                return $(targetElement);
            } else {
                return element.next();
            }
        }

        function isDisplayed(element) {
            var targetElement = getTargetElement(element);
            return targetElement.css('display') != 'none';
        }

        function getToggleIconClassName(isShow) {
            return isShow ? settings.iconShow : settings.iconHide;
        }

        $(this).each(init);

        return $(this);
    };
})(jQuery);
