$(function(){
    // tooltip
    if ($('[rel=tooltip]').length) {
        $('[rel=tooltip]')
        .tooltip()
        .live('click', function() {
            $('.tooltip').remove();
        });
    }

    // flash message
    if ($('.alert-message').length) {
        $('.alert-message').alert();
    }
    
    // ajax loader
    var ajaxLoaderElement = $('#ajax-loader');
    if (ajaxLoaderElement.length) {
        ajaxLoaderElement
            .bind('ajaxSend', function(){
                $(this).show();
                $('body').css('cursor', 'wait');
            })
            .bind('ajaxComplete', function(){
                $(this).hide();
                $('body').css('cursor', 'auto');
            });
    }
});
