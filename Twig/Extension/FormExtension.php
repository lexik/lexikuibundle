<?php

namespace Lexik\Bundle\UiBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormRendererInterface;
use Symfony\Component\Translation\Translator;

/**
 * Generate translation keys as form labels.
 *
 * @author Cédric Girard <c.girard@lexik.fr>
 * @author Jeremy Barthe <j.barthe@lexik.fr>
 */
class FormExtension extends \Twig_Extension
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var Kernel
     */
    private $kernel;

    /**
     * @var Translator
     */
    private $translator;

    /**
     * @var FormRendererInterface
     */
    private $renderer;

    /**
     * @var string
     */
    private $prefixName;

    /**
     * Construct.
     *
     * @param FormRendererInterface $renderer
     * @param string $prefixName
     */
    public function __construct(FormRendererInterface $renderer, $prefixName)
    {
        $this->renderer = $renderer;
        $this->prefixName = !empty($prefixName) ? $prefixName.'.' : '';
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            'form_get_translated_label' => new \Twig_Function_Method($this, 'renderLabelAsText', array('is_safe' => array('html'))),
        );
    }

    /**
     * Configure optional services.
     *
     * @param ContainerInterface $container
     */
    public function configure(ContainerInterface $container)
    {
        $this->container  = $container;
        $this->kernel     = $container->get('kernel');
        $this->translator = $container->get('translator');
    }

    /**
     * Set prefix name.
     *
     * @param string $prefixName
     */
    public function setPrefixName($prefixName)
    {
        $this->prefixName = $prefixName;
    }

    /**
     * Returns the translated label for the given view.
     *
     * @param FormView $view
     * @param string   $label
     * @param array    $variables
     *
     * @return string The translated label
     */
    public function renderLabelAsText(FormView $view, $label = null, array $variables = array())
    {
        if ($this->renderer->humanize($view->vars['name']) === $label || null === $label) {
            $this->updateFormView($view, $label, $variables);
        }

        if (null !== $this->translator) {
            if (isset($view->vars['label_key'])) {
                $label = $view->vars['label_key'];
                $domain = $view->vars['key_translation_domain'];
            } else {
                $domain = $view->vars['translation_domain'];
            }

            $label = $this->translator->trans($label, array(), $domain);
        }

        return $label;
    }

    /**
     * Compute the translation key and update the FormView.
     *
     * @param FormView $view
     * @param string   $label
     * @param array    $variables
     *
     * @return array
     */
    protected function updateFormView(FormView $view, $label = null, array $variables = array())
    {
        $view->vars['label_key'] = null;
        $view->vars['key_translation_domain'] = $this->getTranslationDomain($view->vars['translation_domain']);

        if (null !== $view->parent) {
            $labels = $this->getParentLabels($view);
            $firstLabel = $labels[0];
            unset($labels[0]);

            $view->vars['label_key'] = strtolower(sprintf('%s%s%s%s%s',
                ($firstLabel ? sprintf('%s.', $firstLabel) : ''),
                $this->prefixName,
                implode('.', $labels),
                (count($labels) > 0) ? '.' : '',
                isset($view->vars['label']) ? $view->vars['label'] : $view->vars['name']
            ));
        }
    }

    /**
     * Get the translation domain from the requested controller.
     *
     * @param string $domain
     * @return string
     */
    protected function getTranslationDomain($domain = 'messages')
    {
        // keep customized domain
        if ('messages' != $domain) {
            return $domain;
        }

        $request = (null != $this->container) ? $this->container->get('request') : null;

        if ( ! $request instanceof Request ) {
            return $domain;
        }

        // auto customize domain according to the controller
        $controller = $request->attributes->get('_controller');

        if (class_exists($controller)) {
            $domain = $this->getBundleForClass($controller)->getName();
        } else {
            $pieces = explode(':', $controller);
            if (isset($pieces[0])) {
                if (class_exists($pieces[0])) {
                    $domain = $this->getBundleForClass($pieces[0])->getName();
                } else { // controller as service
                    try {
                        $serviceController = $this->container->get($pieces[0]);
                        $domain = $this->getBundleForClass(get_class($serviceController))->getName();
                    } catch (ServiceNotFoundException $e) {}
                }
            }
        }

        return $domain;
    }

    /**
     * Returns the Bundle instance in which the given class name is located.
     * Thanks to SensioFrameworkExtraBundle.
     *
     * @param string $class A fully qualified controller class name
     * @param Bundle $bundle A Bundle instance
     * @throws \InvalidArgumentException
     */
    protected function getBundleForClass($class)
    {
        $namespace = strtr(dirname(strtr($class, '\\', '/')), '/', '\\');
        foreach ($this->kernel->getBundles() as $bundle) {
            if (0 === strpos($namespace, $bundle->getNamespace())) {
                return $bundle;
            }
        }

        throw new \InvalidArgumentException(sprintf('The "%s" class does not belong to a registered bundle.', $class));
    }

    /**
     * Return an array with parent types label.
     *
     * @param FormView $view
     */
    protected function getParentLabels(FormView $view)
    {
        $parentLabels = array();
        $parent = $view->parent;

        while (null !== $parent) {
            $grandParent = $parent->parent;
            if (null == $grandParent || (null != $grandParent && !in_array('collection', $grandParent->vars['block_prefixes'])) ) { // skip name with numeric chars
                array_unshift($parentLabels, $parent->vars['name']);
            }
            $parent = $grandParent;
        }

        return $parentLabels;
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'lexik_ui_form';
    }
}
